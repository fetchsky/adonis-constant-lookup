"use strict";
const path = require("path");

module.exports = async function(cli) {
  try {
    await cli.makeConfig(
      "constant.js",
      path.join(__dirname, "./templates/config.js")
    );
    cli.command.completed("✔ create", "config/constant.js");

    let destPath = new Date().getTime() + "_constant_lookup.js";
    await cli.copy(
      path.join(__dirname, "./templates/migration.js"),
      cli.helpers.migrationsPath(destPath)
    );
    cli.command.completed("✔ create", "database/migrations/" + destPath);
  } catch (error) {
    // ignore errors
  }
};
