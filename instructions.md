## Registering provider

Make sure to register this plugin's provider. The providers are registered inside `start/app.js`

```js
const providers = ["adonis-constant-lookup/providers/ConstantProvider"];
```
