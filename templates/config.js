"use strict";

const Env = use("Env");

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Database configuration
  |--------------------------------------------------------------------------
  | Connection and table name where constant lookup exists
  */
  connection: Env.get("DB_CONNECTION", "primary"),
  table: Env.get("CONSTANT_TABLE", "constant_lookups"),
  /*
  |--------------------------------------------------------------------------
  | Groups
  |--------------------------------------------------------------------------
  | The list of supported groups for this app. Set this field to an empty 
  | array to support all possible groups
  */
  groups: [],

  /*
  |--------------------------------------------------------------------------
  | Autoload
  |--------------------------------------------------------------------------
  | This field allows you to control when to autoload constant lookup.
  | Currently it only supports two events which are listed below
  */
  autoload: {
    beforeHttpServer: true,
    beforeAceCommand: false
  }
};
