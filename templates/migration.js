"use strict";

const Config = use("Config");
const Schema = use("Schema");

class ConstantSchema extends Schema {
  static get connection() {
    return Config.get("constant.connection");
  }

  up() {
    this.create(Config.get("constant.table"), table => {
      table.increments();
      table
        .string("key", 100)
        .notNullable()
        .unique();
      table.string("value", 500).notNullable();
      table
        .enum("data_type", ["STRING", "NUMBER", "BOOLEAN", "JSON", "DATE"])
        .default("STRING")
        .notNullable();
      table
        .string("group", 32)
        .notNullable()
        .default("N/A");
      table
        .boolean("is_active")
        .notNullable()
        .default(true);
      table.datetime("deleted_at").default(null);
      table.timestamps();
      table.unique(["group", "key"], "group_key_unique");
    });
  }

  async down() {
    this.drop(Config.get("constant.table"));
  }
}

module.exports = ConstantSchema;
