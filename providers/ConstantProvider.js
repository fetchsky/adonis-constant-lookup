"use strict";

const { hooks } = use("@adonisjs/ignitor");
const { ServiceProvider } = use("@adonisjs/fold");

class ConstantProvider extends ServiceProvider {
  register() {
    this.app.singleton("Constant", app => {
      const ConstantLookup = require("../src/ConstantLookup");
      return new ConstantLookup();
    });
    hooks.before.httpServer(() => {
      const ConstantLookup = use("Constant");
      ConstantLookup.load();
    });
  }
}

module.exports = ConstantProvider;
