"use strict";

const Event = use("Event");
const Config = use("Config");
const moment = use("moment");
const Database = use("Database");
const { isObject, isArray, flattenDeep, last } = use("lodash");

class Constant {
  async load() {
    if (!isLoaded) {
      await this.refresh();
    }
    return true;
  }

  get groups() {
    if (isArray(groups) && groups.length) {
      return groups;
    }
    let _groups = Config.get("constant.groups");
    if (isArray(_groups) && _groups.length) {
      groups = _groups;
    }
    return groups;
  }

  async refresh() {
    let query = Database.connection(Config.get("constant.connection"))
      .from(Config.get("constant.table"))
      .select(["key", "value", "data_type", "group"])
      .where("is_active", true)
      .whereNull("deleted_at");
    if (this.groups.length) {
      query.whereIn("group", this.groups);
    }
    const records = await query;
    constantData = {};
    constantByGroups = {};
    records.forEach((record) => {
      if (!constantByGroups[record.group + ""]) {
        constantByGroups[record.group + ""] = {};
      }
      constantByGroups[record.group + ""][record.key] = _parseValue(
        record.value,
        record.data_type
      );
      constantData[record.key] = _parseValue(record.value, record.data_type);
    });
    if (this.groups.length) {
      this.groups.forEach((group) => {
        if (constantByGroups[group]) {
          Object.assign(constantData, constantByGroups[group]);
        }
      });
    } else {
      for (let group in constantByGroups) {
        Object.assign(constantData, constantByGroups[group]);
      }
    }
    isLoaded = true;
    Event.emit("CONSTANT::LOADED", true);
  }

  /**
   * This method is used to get value(s) by key(s)
   *
   * @param {String|Array} key
   * @param {String} [defaultValue=null]
   * @param {String} [group='N/A']
   * @returns {String|Object}
   */
  get(key, defaultValue = null, group = null) {
    if (typeof key === "undefined") {
      return { ...constantData };
    }
    let result = {};
    if (group !== null) {
      group = parseGroupName(group);
    }
    let _keys = isArray(key) ? key : [key];
    for (let i = 0; i < _keys.length; i++) {
      let value;
      if (typeof group === "string") {
        if (constantByGroups[group]) {
          value = constantByGroups[group][_keys[i]];
        }
      } else {
        value = constantData[_keys[i]];
      }
      if (value === undefined) {
        result[_keys[i]] = defaultValue;
      } else {
        result[_keys[i]] = value;
      }
    }
    if (typeof key === "string") {
      return result[key];
    }
    return result;
  }

  async set(key, value = null, dataType = "STRING", group = "N/A") {
    if (key === null || value === null) {
      return;
    }
    let _value = _parseValue(value, dataType, true);
    const date = moment().format("YYYY-MM-DD H:m:s");
    group = parseGroupName(group);
    let model = await Database.connection(Config.get("constant.connection"))
      .table(Config.get("constant.table"))
      .select(["id", "key", "value"])
      .where("key", key)
      .where("group", group)
      .first();
    if (model) {
      await Database.connection(Config.get("constant.connection"))
        .table(Config.get("constant.table"))
        .select(["id", "key", "value"])
        .where("id", model.id)
        .update({
          value: _value,
          is_active: true,
          data_type: dataType,
          deleted_at: null,
          updated_at: date,
        });
    } else {
      await Database.connection(Config.get("constant.connection"))
        .table(Config.get("constant.table"))
        .insert({
          key,
          group,
          value: _value,
          is_active: true,
          data_type: dataType,
          deleted_at: null,
          created_at: date,
          updated_at: date,
        });
    }
    constantData[key] = value;
    if (constantByGroups[group]) {
      constantByGroups[group][key] = value;
    }
  }

  /**
   * This method is used to unset constants
   *
   * @param {Array} keys
   * @param {String} group
   */
  async unset(keys, group = "N/A") {
    let removableKeys = [];
    if (keys.length) {
      flattenDeep(keys).forEach((key) => {
        removableKeys.push(key + "");
      });
    }
    group = parseGroupName(group);
    const date = moment().format("YYYY-MM-DD H:m:s");
    await Database.connection(Config.get("constant.connection"))
      .table(Config.get("constant.table"))
      .whereIn("key", removableKeys)
      .where("group", group)
      .update({
        is_active: false,
        updated_at: date,
      });
    removableKeys.forEach((key) => {
      delete constantData[key];
    });
  }

  /**
   * This method is used to soft-delete constants
   *
   * @param {Array} keys
   * @param {String} group
   */
  async softDelete(keys, group = "N/A") {
    let removableKeys = [];
    if (keys.length) {
      flattenDeep(keys).forEach((key) => {
        removableKeys.push(key + "");
      });
    }
    group = parseGroupName(group);
    const date = moment().format("YYYY-MM-DD H:m:s");
    await Database.connection(Config.get("constant.connection"))
      .table(Config.get("constant.table"))
      .whereIn("key", removableKeys)
      .where("group", group)
      .update({
        deleted_at: date,
        updated_at: date,
      });
    removableKeys.forEach((key) => {
      delete constantData[key];
    });
  }

  /**
   * This method is used to hard-delete constants
   *
   * @param {Array} keys
   * @param {String} group
   */
  async remove(keys, group = "N/A") {
    let removableKeys = [];
    if (keys.length) {
      flattenDeep(keys).forEach((key) => {
        removableKeys.push(key + "");
      });
    }
    group = parseGroupName(group);
    await Database.connection(Config.get("constant.connection"))
      .table(Config.get("constant.table"))
      .whereIn("key", removableKeys)
      .where("group", group)
      .delete();
    removableKeys.forEach((key) => {
      delete constantData[key];
    });
  }

  get isLoaded() {
    return isLoaded;
  }
}

var groups = [];
var isLoaded = false;
var constantData = {};
var constantByGroups = {};

/**
 * This method is used to parse value according to the given constantData type
 *
 * @param value
 * @param {String} [dataType='STRING']
 * @param {Boolean} [reverse=false]
 * @returns {*}
 */
function _parseValue(value, dataType = "STRING", reverse = false) {
  let _value;
  if (reverse) {
    if (value instanceof Date) {
      _value = value.toISOString();
    } else if (value && isObject(value)) {
      try {
        _value = JSON.stringify(value);
      } catch (error) {
        _value = "";
      }
    } else if (dataType === "BOOLEAN") {
      _value = value ? "TRUE" : "FALSE";
    } else {
      _value = value + "";
    }
  } else {
    if (dataType === "STRING") {
      _value = value;
    } else if (dataType === "NUMBER") {
      _value = isNaN(value) ? 0 : parseFloat(value);
    } else if (dataType === "BOOLEAN") {
      _value = !value || value.toUpperCase() !== "TRUE" ? false : true;
    } else if (dataType === "DATE") {
      _value = new Date(value);
    } else if (dataType === "JSON") {
      try {
        _value = JSON.parse(value);
      } catch (e) {
        _value = {};
      }
    }
  }
  return _value;
}

function parseGroupName(group) {
  if (isArray(groups) && groups.length) {
    let groupIndex = groups.indexOf(group);
    if (groupIndex < 0) {
      group = last(groups);
    }
  }
  return group + "";
}
module.exports = Constant;
